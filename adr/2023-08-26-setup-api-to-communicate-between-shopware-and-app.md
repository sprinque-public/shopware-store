---
title: Setup API to communicate between shopware and app
date: 2023-08-26
area: Resources
tags: [script, customer, token]
---
## Context

We need something to communicate between the shopware and the App.

## Decision

To secure the API, we need to generate a token for the customer in the shopware 
and then send the ID and token of customer to the App. 
The App will verify whether the token is valid or not

## Handle

1. Create a script in the shopware app to generate token via customerId
2. Send customerId and this token to Shopware via API (csrf_token)
3. The App instance will verify the token and customerId to decide whether should allow this request or not

## How to use:

- Load appUrl from script and add it to the page object:
- Import AppClient `src/service/app-client.service.js`
- Create AppClient instance `this.appClient = new AppClient(shopId, appUrl)`
- Do an API call

Example:

```twig
{% import "include/load-app-url.twig" as loadAppUrl %}
{% set sprinqueApp = {
    'appUrl': loadAppUrl.appUrl(),
} %}

{% do hook.page.addArrayExtension('sprinqueApp', sprinqueApp) %}
```
And then get it from the page object

```twig
page.getExtension('sprinqueApp').get('appUrl')
```

Call API from JS:

```js
import AppClient from 'path/to/src/service/app-client.service.js'

class AnyPlugin extends Plugin {
    init() {
        this.appClient = new AppClient(shopId, appUrl)
    }

    callApi() {
        this.appClient.post('/endpoint', { payload: 'any thing' })
    }
}
```
