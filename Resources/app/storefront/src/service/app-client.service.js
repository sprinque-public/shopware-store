import { joinPath } from '../util/helper.util';

let controller = null;

export default class AppClient {
    constructor(shopId, shopUrl = '') {
        this.setShopId(shopId);
        this.setShopUrl(shopUrl);
    }

    get(endpoint, options = {}) {
        options.method = 'GET';

        return this.request(endpoint, options);
    }

    post(endpoint, data = null, options = {}) {
        options.method = 'POST';
        if (data) {
            options.body = JSON.stringify(data);
        }

        return this.request(endpoint, options);
    }

    patch(endpoint, data = null, options = {}) {
        options.method = 'PATCH';
        if (data) {
            options.body = JSON.stringify(data);
        }

        return this.request(endpoint, options);
    }

    put(endpoint, data = null, options = {}) {
        options.method = 'PATCH';
        if (data) {
            options.body = JSON.stringify(data);
        }

        return this.request(endpoint, options);
    }

    delete(endpoint, options = {}) {
        options.method = 'DELETE';

        return this.request(endpoint, options);
    }

    async request(endpoint, options = {}) {
        if (!options.headers) {
            options.headers = {};
        }

        const {
            token,
            customerId,
            salesChannelId,
            currencyId,
            languageId,
            billingAddressId,
            shippingAddressId,
        } = await this.prepareCustomerToken();

        options.headers = {
            'accept': 'application/json,',
            'Content-Type': 'application/json',
            'pl-shopware-app-shop-id': this.shopId,
            'sw-customer-token': token,
            'sw-customer-id': customerId,
            'sw-sales-channel-id': salesChannelId,
            'sw-currency-id': currencyId,
            'sw-language-id': languageId,
            'sw-billing-address-id': billingAddressId,
            'sw-shipping-address-id': shippingAddressId,
            ...options.headers,
        };

        controller = new AbortController();
        options.signal = controller.signal;

        const response = await fetch(joinPath(this.shopUrl, 'storefront-api', endpoint), options);

        controller = null;

        if (!response.ok) {
            return Promise.reject(await response.json());
        }

        return response.json();
    }

    async prepareCustomerToken() {
        const url = window['sprinqueRoute']['frontend.storefront.sprinque.token'];

        controller = new AbortController();

        const response = await fetch(url, {
            signal: controller.signal,
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
            },
        });

        controller = null;

        if (!response.ok) {
            return Promise.reject();
        }

        const payload = await response.json();

        return Promise.resolve(payload);
    }

    setShopId(shopId) {
        this.shopId = shopId;
    }

    setShopUrl(shopUrl) {
        this.shopUrl = shopUrl;
    }

    abort() {
        return new Promise((resolve) => {
            if (controller) {
                controller.abort();

                setTimeout(function () {
                    resolve();
                }, 0);

                return;
            }
            resolve();
        });
    }
}
