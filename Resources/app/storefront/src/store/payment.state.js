window.sprinquePayment = {};

export default {
    listener(val) {
    },

    registerListener(listener) {
        this.listener = listener;
    },

    getPayment() {
        return window.sprinquePayment;
    },

    setPayment(payload) {
        window.sprinquePayment = { ...payload };
        this.listener(window.sprinquePayment);
    },

    updatePayment(field, value) {
        window.sprinquePayment = {
            ...this.getPayment(),
            [field]: value,
        };
        this.listener(window.sprinquePayment);
    },
};
