window.sprinqueForms = {};

export default {
    listener(val) {
    },

    registerListener(listener) {
        this.listener = listener;
    },

    getForm(form) {
        return window.sprinqueForms[form];
    },

    getForms() {
        return window.sprinqueForms;
    },

    setForms(payload) {
        window.sprinqueForms = { ...payload };
        this.listener(window.sprinqueForms);
    },

    updateForm(form, payload) {
        const formPayload = this.getForm(form) || {};

        window.sprinqueForms = {
            ...this.getForms(),
            [form]: {
                ...formPayload,
                ...payload,
            },
        };
        this.listener(window.sprinqueForms);
    },
};


