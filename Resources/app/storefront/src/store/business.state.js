window.sprinqueBusiness = {};

export default {
    listener(val) {
    },

    registerListener(listener) {
        this.listener = listener;
    },

    getBusiness() {
        return window.sprinqueBusiness;
    },

    setBusiness(payload) {
        window.sprinqueBusiness = { ...payload };
        this.listener(window.sprinqueBusiness);
    },

    updateBusiness(payload) {
        window.sprinqueBusiness = {
            ...this.getBusiness(),
            ...payload,
        };
        this.listener(window.sprinqueBusiness);
    },

    hasBusiness() {
        return Object.keys(window.sprinqueBusiness).length > 0;
    }
};
