import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';
import Debouncer from 'src/helper/debouncer.helper';
import businessState from '../store/business.state';

export default class SprinqueRegistrationNumberValidation extends Plugin {
    static options = {
        regNumberInputSelector: '#registration_number',
        warningMsgContainerSelector: '.srpinque-warning',
        warningMsgSelector: '.sprinque-warning .alert-content',
        countrySelectSelector: '#country',
        languageCode: 'en',
    }
    init() {
        this.paymentModal = window.PluginManager.getPluginInstances('SprinquePaymentModal')[0];
        this.regNumberInput = DomAccess.querySelector(this.el, this.options.regNumberInputSelector, false);
        this.warningMsgContainer = DomAccess.querySelector(this.el, this.options.warningMsgContainerSelector, false);
        this.warningMsgEl = DomAccess.querySelector(this.el, this.options.warningMsgSelector, false);

        this.registerEvents();
    }

    registerEvents() {
        const event = Debouncer.debounce(this.onRegNumberChange.bind(this), 500);

        this.regNumberInput.removeEventListener('change', event);
        this.regNumberInput.addEventListener('input', event);

        document.$emitter.subscribe('Sprinque/countryChanged', this._onCountryChange.bind(this));
    }

    /**
     * Handle on country change
     * @private
     */
    _onCountryChange() {
        this.regNumberInput.value = '';
        this.warningMsgContainer.classList.add('d-none');
    }

    /**
     * Handle on registration number change
     * @param event
     */
    onRegNumberChange(event) {
        const regNumberInput = event.target;
        const inputVal = event.target.value;

        // If registration number is empty, remove validation
        if (!event.target.value) {
            this.warningMsgContainer.classList.add('d-none');
            return;
        }

        const countryCode = this._getCountryCode();
        if (!countryCode) return;

        // Validate registration number
        const { isValid, message } = window.Sprinque.checkRegNumber(event.target.value, countryCode, this.options.languageCode);

        // If registration number is invalid, show error message
        if (!isValid) {
            this.warningMsgEl.innerText = message;
            this.warningMsgContainer.classList.remove('d-none');
        } else {
            this.warningMsgContainer.classList.add('d-none');
        }

        // Remove credit bureau id from business state if registration number is changed
        const business = { ...businessState.getBusiness() };

        if (inputVal === business.registration_number) return;

        delete business.credit_bureau_id;
        businessState.setBusiness(business);
    }

    /**
     * Get country code from selected country
     * @returns {*}
     * @private
     */
    _getCountryCode() {
        const countrySelect = this._getCountrySelect();

        return countrySelect.value;
    }

    /**
     * Get country select element
     * @returns {*}
     * @private
     */
    _getCountrySelect() {
        const modalContent = this.paymentModal.getModalContent();
        return DomAccess.querySelector(modalContent, this.options.countrySelectSelector, false);
    }
}
