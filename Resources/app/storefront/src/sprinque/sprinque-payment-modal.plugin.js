import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';
import ButtonLoadingIndicator from 'src/utility/loading-indicator/button-loading-indicator.util';
import PageLoadingIndicatorUtil from 'src/utility/loading-indicator/page-loading-indicator.util';
import PseudoModalUtil from 'src/utility/modal-extension/pseudo-modal.util';
import HttpClient from 'src/service/http-client.service';
import { joinPath } from '../util/helper.util';
import AppClient from '../service/app-client.service';
import formState from '../store/form.state';
import paymentState from '../store/payment.state';

export default class SprinquePaymentModal extends Plugin {
    static options = {
        baseUrl: '',
        shopId: null,
        appUrl: null,
        orderId: null,
        totalAmount: null,
        exception: {},
        logoSelector: '#sprinque-seller-logo',
        modalContentSelector: '#sprinque-modal-content',
        submitPaymentButtonId: '#confirmOrderForm button[type="submit"]',
        paymentFormId: '#confirmOrderForm',
        defaultContentSelector: '',
        modalContentCls: '.sprinque-payment-modal-content',
        modalTemplateCls: '.sprinque-unstyled-modal',
        modalContentTemplateCls: '.sprinque-unstyled-modal-content-element',
        paymentModalCls: 'sprinque-unstyled-modal',
        steps: [], // This is where you can set the associated plugin and options for each step
    };

    init() {
        const {
            shopId,
            appUrl,
            submitPaymentButtonId,
            paymentFormId,
        } = this.options;

        if (!shopId) {
            throw new Error('ShopID not found');
        }

        if (!appUrl) {
            throw new Error('App Url not found');
        }

        this.appClient = new AppClient(shopId, appUrl);
        this.httpClient = new HttpClient();
        this.submitPaymentButton = DomAccess.querySelector(document, submitPaymentButtonId, false);
        this.submitButtonLoader = new ButtonLoadingIndicator(this.submitPaymentButton);
        this.paymentForm = DomAccess.querySelector(document, paymentFormId, false);

        this.registerEvents();
    }

    registerEvents() {
        // Submit payment form handler
        this.submitPaymentButton.addEventListener('click', this.onSubmitPayment.bind(this));
    }

    /**
     * Handle open payment modal
     * @param event
     */
    onSubmitPayment(event) {
        event.preventDefault();

        // checks form validity before submit
        if (!this.paymentForm.checkValidity()) {
            return;
        }

        PageLoadingIndicatorUtil.create();
        this._openModal(this.options.defaultContentSelector).catch(() => {
            PageLoadingIndicatorUtil.remove();
        });
    }

    /**
     * Handle opening modal
     * @param selector
     * @private
     */
    async _openModal(selector) {
        const {
            seller,
            countries,
            buyerDetail,
        } = await this.appClient.post('/prepare-payment');
        const content = await this._getPaymentModal({
            logo: seller?.logo,
            name: seller?.name,
            buyerDetail,
            countries,
        });

        if (!content) {
            return;
        }

        this.modal = new PseudoModalUtil(
            content,
            true,
            this.options.modalTemplateCls,
            this.options.modalContentTemplateCls,
        );

        PageLoadingIndicatorUtil.remove();
        this.modal.open(this.setContent.bind(this, selector));
    }

    _getPaymentModal(payload) {
        return new Promise((resolve) => {
            this.httpClient.post(joinPath(this.options.baseUrl, '/storefront/script/sprinque-modal'),
                JSON.stringify(payload),
                (content) => {
                    if (!content) {
                        resolve(null);
                        return;
                    }

                    resolve(content);
                },
            );
        });
    }

    /**
     * Update modal content and initialize plugin if there's any
     * @param contentSelector
     * @param options
     */
    setContent(contentSelector, options = {}) {
        const stepConfig = this.options.steps.find((step) => step.selector === contentSelector);
        const modal = this.modal.getModal();
        if (!modal || !stepConfig) {
            return;
        }

        const contentEl = DomAccess.querySelector(modal, contentSelector);
        const stepEleList = DomAccess.querySelectorAll(modal, this.options.modalContentCls);
        const logoEl = DomAccess.querySelector(modal, this.options.logoSelector);

        const { hideLogo = false } = stepConfig;
        if (hideLogo) {
            logoEl.classList.add('d-none');
        } else {
            logoEl.classList.remove('d-none');
        }

        // Hide all steps
        stepEleList.forEach((el) => {
            el.classList.add('d-none');
        });

        // Show the selected step
        contentEl.classList.remove('d-none');
        this.content = contentEl;
        this._onOpen(this.modal, contentSelector, stepConfig, options);
    }

    appendModalContent(modalContent) {
        const modal = this.modal.getModal();
        if (!modal) {
            return;
        }

        const modalContentEl = DomAccess.querySelector(modal, this.options.modalContentSelector);
        modalContentEl.insertAdjacentHTML('beforeend', modalContent);
    }

    /**
     * Handle after opening modal event
     * @param modal
     * @param contentSelector
     * @param config
     * @param options
     * @private
     */
    _onOpen(modal, contentSelector, config, options = {}) {
        const modalEl = modal.getModal();
        const modalContent = DomAccess.querySelector(modalEl, this.options.modalContentTemplateCls, false);

        // If there's a plugin associated with the modal content, initialize the plugin
        this._initPlugin(config, contentSelector, modalContent, options);

        // Attach close event listener
        modalEl.addEventListener('hidden.bs.modal', this._onClose.bind(this, modal));

        // If there's extra class to be set for the modal content, set it
        if (this.options.paymentModalCls) {
            modalEl.classList.add(this.options.paymentModalCls);
        }

        // Publish onOpen event for other plugins to subscribe
        this.$emitter.publish('Sprinque/onModalOpen', { modal });
    }

    /**
     * Get visible modal step content
     * @returns {*}
     */
    getModalContent() {
        return this.content;
    }

    /**
     * Initialize plugin for the modal content
     * @param config
     * @param contentSelector
     * @param content
     * @param options
     * @private
     */
    _initPlugin(config, contentSelector, content, options = {}) {
        if (!config) return;

        const {
            baseUrl,
            shopId,
            appUrl,
            orderId,
            totalAmount,
            paymentFormId,
            exception,
        } = this.options;

        const {
            pluginName,
            pluginOptions = {},
        } = config;
        window.PluginManager.initializePlugin(pluginName, content, {
            baseUrl,
            shopId,
            appUrl,
            orderId,
            totalAmount,
            paymentFormId,
            contentSelector,
            exception,
            ...pluginOptions,
            ...options,
        });

        // Initialize other plugins
        window.PluginManager.initializePlugins();
    }

    /**
     * Handle onClose modal event
     * @param pseudoModal
     * @private
     */
    _onClose(pseudoModal) {
        this.removeButtonLoading();

        formState.setForms({});
        paymentState.setPayment({});

        this.$emitter.publish('Sprinque/onModalClose', { pseudoModal });
    }

    createButtonLoading() {
        this.submitButtonLoader.create();
    }

    removeButtonLoading() {
        this.submitButtonLoader.remove();
    }
}
