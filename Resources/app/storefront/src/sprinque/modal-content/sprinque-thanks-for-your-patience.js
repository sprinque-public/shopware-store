import deepmerge from 'deepmerge';
import SprinqueModalContentCore from '../../core/sprinque-modal-content.core';
import { MANUAL_REVIEW, APPROVED } from '../../util/constants.util';
import { joinPath } from '../../util/helper.util';
import paymentState from '../../store/payment.state';

export default class SprinqueThanksForYourPatience extends SprinqueModalContentCore {
    static options = deepmerge(SprinqueModalContentCore.options, {
        reject: {
            title: '',
            description: '',
        },
        rejectDescription: '',
        manualReviewModalSelector: '#manual-review',
        paymentTermModalSelector: '#payment-terms',
    });

    init() {
        super.init();
        this._getBuyerDetail().catch(this.showException.bind(this));
    }

    /**
     * Call API to get buyer detail to decide which modal content should show
     */
    async _getBuyerDetail() {
        const {
            reject,
            totalAmount,
        } = this.options;
        const {
            buyer_detail,
            pricing,
        } = await this.appClient.post('/buyer/detail', {
            buyerId: paymentState.getPayment()?.buyerId,
        });
        const { credit_qualification } = buyer_detail || {};
        const {
            credit_decision,
            eligible_payment_terms,
            available_credit_limit,
        } = credit_qualification || {};

        // Move to manual review modal if `credit_decision` is MANUAL_REVIEW
        if (credit_decision === MANUAL_REVIEW) {
            this.paymentModal.setContent(this.options.manualReviewModalSelector);
            return;
        }

        if (available_credit_limit < totalAmount) {
            this.showErrorModal(reject.title, reject.description);
            return;
        }

        // Move to payment term selection modal if `credit_decision` is APPROVED
        if (credit_decision === APPROVED) {
            const content = await this._getTermsModal({
                eligible_payment_terms,
                pricing,
                totalAmount,
            });

            // Append select term content to modal
            this.paymentModal.appendModalContent(content);
            this.paymentModal.setContent(this.options.paymentTermModalSelector);
            return;
        }

        // Show reject modal
        this.showErrorModal(reject.title, reject.description);
    }

    /**
     * Call API to get terms modal content
     */
    _getTermsModal(payload) {
        return new Promise((resolve, reject) => {
            this.httpClient.post(joinPath(this.options.baseUrl, '/storefront/script/sprinque-terms'),
                JSON.stringify(payload),
                (content) => {
                    if (!content) {
                        reject(null);
                        return;
                    }

                    resolve(content);
                },
            );
        });
    }
}
