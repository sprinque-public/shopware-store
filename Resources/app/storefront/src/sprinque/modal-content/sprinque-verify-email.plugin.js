import deepmerge from 'deepmerge';
import DomAccess from 'src/helper/dom-access.helper';
import ElementLoadingIndicatorUtil from 'src/utility/loading-indicator/element-loading-indicator.util';

import SprinqueModalContentCore from '../../core/sprinque-modal-content.core';

export default class SprinqueVerifyEmail extends SprinqueModalContentCore {
    static options = deepmerge(SprinqueModalContentCore.options, {
        otpInputContainer: '#otp-input',
        resendBtnSelector: '#resend-btn',
        resendTimerSelector: '#resend-timer',
        timerTextSelector: '#timer-text',
        timerDuration: 30,
        invalidCls: 'invalid',
    });

    init() {
        super.init();

        this.otpInputContainer = DomAccess.querySelector(document, this.options.otpInputContainer);
        this.resendBtn = DomAccess.querySelector(this.contentEl, this.options.resendBtnSelector);
        this.timerText = DomAccess.querySelector(this.contentEl, this.options.timerTextSelector);

        this.registerEvents();
    }

    registerEvents() {
        this.otpInputContainer.addEventListener('input', this.onInputOTP.bind(this));
        this.otpInputContainer.addEventListener('keyup', this.onKeyPress.bind(this));
        this.otpInputContainer.addEventListener('paste', this.onPaste.bind(this));
        this.resendBtn.addEventListener('click', this.onResendBtnClick.bind(this));

        this.startTimer(
            this.options.timerDuration,
            DomAccess.querySelector(this.contentEl, this.options.resendTimerSelector),
        );
        this.$emitter.subscribe('Sprinque/otpChanged', this._onOTPChanged.bind(this));
    }

    /**
     * Handle input event
     * @param e
     */
    onInputOTP(e) {
        this.$emitter.publish('Sprinque/otpChanged');

        const target = e.target;
        const val = target.value;

        // If input is not a number, clear it
        if (isNaN(val)) {
            target.value = '';
            return;
        }

        // If input has value, focus next input
        if (val) {
            const next = target.nextElementSibling;
            if (next) {
                next.focus();
            }
        }
    }

    /**
     * Handle key press event
     * @param e
     */
    onKeyPress(e) {
        this.$emitter.publish('Sprinque/otpChanged');

        const target = e.target;
        const key = e.key.toLowerCase();

        // If user presses backspace or delete, clear input and set focus on previous input
        if (key === 'backspace' || key === 'delete') {
            target.value = '';
            const prev = target.previousElementSibling;
            if (prev) {
                prev.focus();
            }
        }
    }

    /**
     * Handle paste event
     * @param {Event} e
     */
    onPaste(e) {
        e.preventDefault();

        this.$emitter.publish('Sprinque/otpChanged');

        const paste = (e.clipboardData || window.clipboardData).getData('text');
        if (paste) {
            const chars = paste.split('');

            let target = e.target;
            chars.forEach((char) => {
                if (isNaN(char)) return;

                target.value = char;
                if (target.nextElementSibling) {
                    target = target.nextElementSibling;
                }
            });

            target.focus();
        }
    }

    async _onNextBtnClick(e) {
        e.preventDefault();
        this._displayAlertMessage();
        this.clearTimer();

        const otp = this._getOtp();
        if (!otp && otp.length < 5) return;

        try {
            ElementLoadingIndicatorUtil.create(this.el);

            const { email_otp_validated } = await this.appClient.post('/buyer/verify-otp', {
                otp,
                ...this.options.payload,
            });

            if (!email_otp_validated) {
                this._showError();

                return;
            } else {
                this.$emitter.publish('Sprinque/otpChanged');
            }

            this.options.nextModalSelector && this.paymentModal.setContent(this.options.nextModalSelector);
        } catch (e) {
            this.showException(e);
        } finally {
            ElementLoadingIndicatorUtil.remove(this.el);
        }
    }

    /**
     * Start the timer from given duration
     * @param duration
     * @param display
     */
    startTimer(duration, display) {
        let timer = duration, seconds;
        this.interval = setInterval(() => {
            seconds = parseInt(timer % 60, 10);
            if (seconds === 0) {
                this.clearTimer();
            }

            display.textContent = seconds < 10 ? '0' + seconds : seconds;

            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }

    /**
     * Clear the timer and show resend button
     */
    clearTimer() {
        clearInterval(this.interval);
        this.resendBtn.disabled = false;
        this.timerText.classList.add('d-none');
    }

    /**
     * Handle resend button click
     * @param e
     * @returns {Promise<void>}
     */
    async onResendBtnClick(e) {
        e.preventDefault();

        if (!this.options.payload) return;

        try {
            // Disable resend button
            this.resendBtn.disabled = true;

            await this.appClient.post('/buyer/send-otp', this.options.payload);

            // Show and start the timer
            this.timerText.classList.remove('d-none');
            this.startTimer(
                this.options.timerDuration,
                DomAccess.querySelector(this.contentEl, this.options.resendTimerSelector),
            );
        } catch (e) {
            console.error(e);
        }
    }

    /**
     * Get entered OTP value
     * @returns {*}
     * @private
     */
    _getOtp() {
        const inputs = DomAccess.querySelectorAll(this.otpInputContainer, 'input', false);

        return [...inputs].reduce((otp, input) => {
            return otp + input.value;
        }, '');
    }

    /**
     * Show error message and disable next button
     * @private
     */
    _showError() {
        this.otpInputContainer.classList.add(this.options.invalidCls);
        this.nextBtn.disabled = true;
    }

    /**
     * Handle on modal close event
     * @private
     */
    _onModalClose() {
        this.clearTimer();
    }

    /**
     * Handle on OTP changed event
     * @private
     */
    _onOTPChanged() {
        this.otpInputContainer.classList.remove(this.options.invalidCls);
        this.nextBtn.disabled = false;
    }
}
