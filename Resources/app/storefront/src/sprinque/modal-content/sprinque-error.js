import DomAccess from 'src/helper/dom-access.helper';
import SprinqueModalContentCore from '../../core/sprinque-modal-content.core';

export default class SprinqueError extends SprinqueModalContentCore {
    static options = {
        title: null,
        description: null,
        titleSelector: 'h4',
        descriptionSelector: 'p',
    };

    init() {
        super.init();

        this.titleEl = DomAccess.querySelector(this.contentEl, this.options.titleSelector);
        this.descriptionEl = DomAccess.querySelector(this.contentEl, this.options.descriptionSelector);

        this._displayContent();
    }

    /**
     * Set content for the error modal
     * @private
     */
    _displayContent() {
        this.titleEl.innerHTML = this.options.title;
        this.descriptionEl.innerHTML = this.options.description;
    }
}
