import deepmerge from 'deepmerge';
import Debouncer from 'src/helper/debouncer.helper';
import DomAccess from 'src/helper/dom-access.helper';
import SprinqueModalContentCore from '../../core/sprinque-modal-content.core';
import ElementLoadingIndicatorUtil from '../../util/element-loading-indicator.util';
import { joinPath } from '../../util/helper.util';
import businessState from '../../store/business.state';

export default class SprinqueSelectBusiness extends SprinqueModalContentCore {
    static options = deepmerge(SprinqueModalContentCore.options, {
        searchSelector: '#sprinque-search-business',
        searchDelay: 250,
        searchMinChars: 3,
        companyResultSelector: '.sprinque-company-result',
        businessItemSelector: '.sprinque-business-list-item',
        registrationNumberSelector: '#registration_number',
        registrationNumberAttr: 'data-registration-number',
        searchInitValueAttr: 'data-init-value',
        businessInfoAttr: 'data-business-info',
        searchTypeSelector: 'input[name="company_type"]',
        companyNameRadioSelector: 'input#type_company_name',
        selectedBusinessCardSelector: '.sprinque-selected-business-card',
        selectedBusinessCardContentSelector: '.sprinque-selected-business-card-content',
        removeBusinessBtnSelector: '.remove-business-btn',
    });
    countryCode = null;
    isSearchByVatAvailable = 0;
    isBusinessSelected = false;
    inputVal = null;
    isResultShown = false;

    init() {
        super.init();

        const {
            searchSelector,
            companyResultSelector,
            registrationNumberSelector,
            companyNameRadioSelector,
        } = this.options;

        this.searchEl = DomAccess.querySelector(this.contentEl, searchSelector);
        this.companyResultEl = DomAccess.querySelector(this.contentEl, companyResultSelector);
        this.registrationNumberEl = DomAccess.querySelector(this.contentEl, registrationNumberSelector);
        this.companyNameRadioEl = DomAccess.querySelector(this.contentEl, companyNameRadioSelector);
        this.removeBusinessBtnEl = DomAccess.querySelector(this.contentEl, this.options.removeBusinessBtnSelector);
        this.selectedBusinessCardEl = DomAccess.querySelector(this.contentEl,
            this.options.selectedBusinessCardSelector,
            false,
        );

        this._registerEvents();
    }

    _registerEvents() {
        this.searchEl.addEventListener('input',
            Debouncer.debounce(this._onSearchInput.bind(this), this.options.searchDelay),
            {
                capture: true,
                passive: true,
            },
        );
        this.searchEl.addEventListener('focus', this._onSearchInput.bind(this));
        this.companyResultEl.addEventListener('click', this._onClickBusinessItem.bind(this));
        this.removeBusinessBtnEl.addEventListener('click', this._onRemoveBusiness.bind(this));

        document.$emitter.subscribe('Sprinque/countryChanged', this._onCountryChange.bind(this));

        businessState.registerListener(this._onBusinessChange.bind(this));
    }

    /**
     * Handle on business change
     * @param business
     * @private
     */
    _onBusinessChange(business) {
        this.isBusinessSelected = Object.keys(business).length > 0;
    }

    /**
     * Handle on country changed
     */
    _onCountryChange(payload) {
        const {
            detail: {
                country,
                isSearchByVatAvailable,
            },
        } = payload;
        if (this.isSearchByVatAvailable !== isSearchByVatAvailable) {
            this.companyNameRadioEl.checked = true;
        }

        this.countryCode = country;
        this.isSearchByVatAvailable = isSearchByVatAvailable;
        this._setBusinessResult();

        // Remove selected business card and business state when country is changed
        this.searchEl.value = this.searchEl.getAttribute(this.options.searchInitValueAttr) || '';
        this._setSelectedBusinessCard();
        businessState.setBusiness({});
    }

    /**
     * Handle on type in search input
     */
    async _onSearchInput(e) {
        e.preventDefault();
        const value = e.target.value.trim();

        // If search value is less than min chars, or business is already selected, or input value is not changed, do nothing
        if (value.length < this.options.searchMinChars || this.isBusinessSelected || (this.isResultShown && this.inputVal === value)) {
            e.stopPropagation();

            return;
        }

        await this._abort();

        this.inputVal = value;
        ElementLoadingIndicatorUtil.create(this.companyResultEl);
        this._handleSearchBusiness(value).finally(() => {
            ElementLoadingIndicatorUtil.remove(this.companyResultEl);
        });
    }

    /**
     * Handle search business
     */
    async _handleSearchBusiness(searchTerm) {
        // Call App API to get business list data
        const { businesses } = await this.appClient.post('/search/business', {
            search_term: searchTerm,
            country_code: this.countryCode,
            search_type: this._getSearchType(),
        });

        // Get business HTML content from business list data
        const businessContent = await this._getBusinessResult({
            businesses,
        });

        this._setBusinessResult(businessContent);
    }

    /**
     * Call shopware API to get business list HTML content
     */
    _getBusinessResult(payload) {
        return new Promise((resolve, reject) => {
            this.httpClient.post(joinPath(this.options.baseUrl, '/storefront/script/sprinque-business-search'),
                JSON.stringify(payload),
                (content) => {
                    if (!content) {
                        reject(null);
                        return;
                    }

                    resolve(content);
                },
            );
        });
    }

    /**
     * Handle on click business item in result list
     */
    _onClickBusinessItem(e) {
        e.preventDefault();
        const {
            businessItemSelector,
            businessInfoAttr,
        } = this.options;
        const businessItemEl = e.target.closest(businessItemSelector);
        if (!businessItemEl) {
            return;
        }

        const businessInfo = JSON.parse(businessItemEl.getAttribute(businessInfoAttr));
        if (businessInfo) {
            businessState.setBusiness(businessInfo);
        }
        this.registrationNumberEl.value = businessInfo?.registration_number ?? '';
        this.searchEl.value = businessInfo?.business_name ?? this.searchEl.value ?? '';

        this._setBusinessResult();
        this._setSelectedBusinessCard(businessInfo);
    }

    /**
     * Set selected business card
     * @param businessInfo
     * @private
     */
    _setSelectedBusinessCard(businessInfo) {
        const selectedBusinessCardContent = DomAccess.querySelector(
            this.selectedBusinessCardEl,
            this.options.selectedBusinessCardContentSelector,
            false,
        );

        if (!businessInfo) {
            selectedBusinessCardContent.innerHTML = '';
            this.selectedBusinessCardEl.classList.add('d-none');
        } else {
            selectedBusinessCardContent.innerHTML = this._createSelectedBusinessCardHtml(businessInfo);
            this.selectedBusinessCardEl.classList.remove('d-none');
        }
    }

    /**
     * Create selected business card HTML
     * @param businessInfo
     * @returns {string}
     * @private
     */
    _createSelectedBusinessCardHtml(businessInfo) {
        const {
            business_name,
            address,
            country,
        } = businessInfo;
        return `<b>${business_name}</b>
            <small>${address.address_line_1 || address.address_line_2}</small>
            <small>${address.zipcode ? `${address.zipcode},` : ''} ${address.city ? `${address.city},` : ''} ${country}</small>`;
    }

    /**
     * Get company search type from input
     */
    _getSearchType() {
        const { searchTypeSelector } = this.options;

        const searchTypeEl = DomAccess.querySelector(this.contentEl, `${searchTypeSelector}:checked`, false);
        if (!searchTypeEl) {
            return null;
        }

        if (!searchTypeEl.value || searchTypeEl.value === 'null') {
            return null;
        }

        return searchTypeEl.value;
    }

    _setBusinessResult(content = '') {
        this.isResultShown = !!content;

        this.companyResultEl.innerHTML = content;
    }

    /**
     * Handle on remove business
     * @private
     */
    _onRemoveBusiness() {
        businessState.setBusiness({});
        this.selectedBusinessCardEl.classList.add('d-none');
    }

    async _abort() {
        await this.appClient.abort();
        await this.httpClient.abort();
    }
}
