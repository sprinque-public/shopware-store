import deepmerge from 'deepmerge';
import { isEqual } from 'lodash';
import DomAccess from 'src/helper/dom-access.helper';
import ElementLoadingIndicatorUtil from 'src/utility/loading-indicator/element-loading-indicator.util';
import SprinqueModalContentCore from '../../core/sprinque-modal-content.core';
import businessState from '../../store/business.state';
import formState from '../../store/form.state';
import paymentState from '../../store/payment.state';

export default class SprinqueCompanyAddress extends SprinqueModalContentCore {
    static options = deepmerge(SprinqueModalContentCore.options, {
        address1Selector: '#address_line_1',
        address2Selector: '#address_line_2',
        citySelector: '#city',
        zipcodeSelector: '#zip_code',
        countryCodeSelector: '#country_code',
        otpModalSelector: '#verify-email',
        merchantBuyerIdSelector: '#merchant_buyer_id',
        thanksForYourPatienceSelector: '#thanks-for-your-patience',
    });

    init() {
        super.init();

        this.businessInfo = businessState.getBusiness();

        this.address1El = DomAccess.querySelector(this.contentEl, this.options.address1Selector, false);
        this.address2El = DomAccess.querySelector(this.contentEl, this.options.address2Selector, false);
        this.cityEl = DomAccess.querySelector(this.contentEl, this.options.citySelector, false);
        this.zipcodeEl = DomAccess.querySelector(this.contentEl, this.options.zipcodeSelector, false);
        this.countryCodeEl = DomAccess.querySelector(this.contentEl, this.options.countryCodeSelector, false);
        this.merchantBuyerIdEl = DomAccess.querySelector(this.contentEl, this.options.merchantBuyerIdSelector, false);

        this._initFormValues();
    }

    /**
     * Populate form values from selected business info
     * @private
     */
    _initFormValues() {
        const {
            address,
            country,
        } = this.businessInfo;
        if (address) {
            this.address1El.value = address.address_line_1 || address.street;
            this.address2El.value = address.address_line_2;
            this.cityEl.value = address.city;
            this.zipcodeEl.value = address.zipcode;
        }

        if (country) {
            this.countryCodeEl.value = country;
        }

        if (businessState.hasBusiness()) {
            this.merchantBuyerIdEl.value = null;
        }
    }

    _onNextBtnClick(e) {
        e.preventDefault();
        this._displayAlertMessage();

        // If address data changed, remove credit bureau id and update business info
        const changed = this._isAddressChanged();
        if (changed) {
            delete this.businessInfo.credit_bureau_id;
            businessState.setBusiness(this.businessInfo);
        }

        const params = this.businessInfo?.credit_bureau_id ? { credit_bureau_id: this.businessInfo.credit_bureau_id } : {};
        this.saveForm(params);

        ElementLoadingIndicatorUtil.create(this.el);
        this._registerBuyer().catch(this.showException.bind(this)).finally(() => {
            ElementLoadingIndicatorUtil.remove(this.el);
        });
    }

    /**
     * Call API to register/verify buyer user
     * and move to correct content depend on the response (user/company status, etc.)
     * @returns {Promise<*>}
     * @private
     */
    async _registerBuyer() {
        const {
            user,
            buyer_detail: {
                buyer_id,
                merchant_buyer_id,
            },
        } = await this.appClient.post('/buyer/register', formState.getForm('buyer'));

        paymentState.updatePayment('buyerId', buyer_id);

        if (user.email_otp_validated) {
            this.paymentModal.setContent(this.options.thanksForYourPatienceSelector);
        } else {
            const payload = {
                merchant_buyer_id,
                email: user.email,
            };

            await this.appClient.post('/buyer/send-otp', payload);
            this.paymentModal.setContent(this.options.otpModalSelector, { payload });
        }

        return user;
    }

    /**
     * Check if address data changed
     * @returns {*}
     * @private
     */
    _isAddressChanged() {
        const { address } = this.businessInfo;
        const formAddress = {
            ...address,
            address_line_1: this.address1El.value,
            address_line_2: this.address2El.value,
            city: this.cityEl.value,
            zipcode: this.zipcodeEl.value,
        };

        return !isEqual(address, formAddress);
    }

    update() {
        this.businessInfo = businessState.getBusiness();

        this._initFormValues();
    }
}
