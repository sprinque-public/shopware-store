import deepmerge from 'deepmerge';
import DomAccess from 'src/helper/dom-access.helper';
import SprinqueModalContentCore from '../../core/sprinque-modal-content.core';
import paymentState from '../../store/payment.state';

export default class SprinquePaymentTerm extends SprinqueModalContentCore {
    static options = deepmerge(SprinqueModalContentCore.options, {
        termRadioSelector: '.term-select input[type="radio"]',
        dataTotalTermAmount: 'data-total-term-amount',
        totalAmountSelector: '#sprinque-total-amount',
    });

    init() {
        super.init();

        const {
            termRadioSelector,
            totalAmountSelector,
        } = this.options;

        this.termRadioEls = DomAccess.querySelectorAll(this.contentEl, termRadioSelector, false);
        this.totalAmountEl = DomAccess.querySelector(this.contentEl, totalAmountSelector);

        this._registerEvents();
    }

    _registerEvents() {
        if (!this.termRadioEls) {
            return;
        }

        if (this.termRadioEls.length > 0) {
            this.termRadioEls.forEach((radio) => {
                radio.addEventListener('change', this._onChangeTermRadio.bind(this));
            });
        }

        if (this.termRadioEls.length === 1) {
            this.termRadioEls[0].checked = true;
            this._onChangeTermRadio({ target: this.termRadioEls[0] });
        }
    }

    /**
     * On select term radio change, set the total order amount and update payment term store
     */
    _onChangeTermRadio({ target }) {
        const totalTermAmount = target.getAttribute(this.options.dataTotalTermAmount);
        if (!totalTermAmount) {
            return;
        }

        paymentState.updatePayment('paymentTerm', target.value);
        this.totalAmountEl.innerHTML = totalTermAmount;
    }

    async _onSubmitButtonClick() {
        await this._submitPayment(true);
    }
}
