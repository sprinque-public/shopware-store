import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';

export default class SprinqueCountrySelect extends Plugin {
    static options = {
        countrySelectSelector: '.sprinque-country-select',
        companyLabelSelector: '.sprinque-company-label',
        companyTypeSelector: '.sprinque-company-type',
        searchByVatAvailableAttr: 'is-search-by-vat-available',
        regNumberRequiredAttr: 'is-registration-number-required',
        regNumberInputSelector: '#registration_number',
        regNumberRequiredSelector: '#registration_number_required'
    }

    init() {
        this.paymentModal = window.PluginManager.getPluginInstances('SprinquePaymentModal')[0];
        this._showSearchTypeSelect(this.el);
        this._setRegNumberInput(this.el);
        if (this.el.value) {
            const isSearchByVatAvailable = this._getIsSearchByVatAvailable(this.el);
            document.$emitter.publish('Sprinque/countryChanged', {
                country: this.el.value,
                isSearchByVatAvailable
            });
        }

        this.registerEvents();
    }

    registerEvents() {
        this.el.addEventListener('change', this.onCountrySelect.bind(this));
    }

    /**
     * Handle on country select
     * @param event
     */
    onCountrySelect(event) {
        this._showSearchTypeSelect(event.target);
        this._setRegNumberInput(event.target);
        const isSearchByVatAvailable = this._getIsSearchByVatAvailable(this.el);

        document.$emitter.publish('Sprinque/countryChanged', {
            country: event.target.value,
            isSearchByVatAvailable
        });
    }

    /**
     * Get is search by vat available from selected country
     * @param el
     * @returns {*}
     * @private
     */
    _getIsSearchByVatAvailable(el) {
        const selectedOption = el.options[el.selectedIndex];
        return DomAccess.getDataAttribute(selectedOption, this.options.searchByVatAvailableAttr) || 0;
    }

    /**
     * Get is registration number required from selected country
     * @param el
     * @returns {*}
     * @private
     */
    _getIsRegistrationNumberRequired(el) {
        const selectedOption = el.options[el.selectedIndex];
        return DomAccess.getDataAttribute(selectedOption, this.options.regNumberRequiredAttr);
    }

    /**
     * Show/hide search type select and is search by vat available
     * @private
     */
    _showSearchTypeSelect(el) {
        const isSearchByVatAvailable = this._getIsSearchByVatAvailable(el);

        // Get modal content and search type select element
        const modalContent = this.paymentModal.getModalContent();
        const companyLabel = DomAccess.querySelector(modalContent, this.options.companyLabelSelector, false);
        const companyType = DomAccess.querySelector(modalContent, this.options.companyTypeSelector, false);

        // Show/hide search type select if is search by vat available in selected country
        if (isSearchByVatAvailable) {
            companyLabel.classList.add('d-none');
            companyType.classList.remove('d-none');
        } else {
            companyLabel.classList.remove('d-none');
            companyType.classList.add('d-none');
        }
    }

    /**
     * Toggle require attribute of registration number input
     * @param el
     * @private
     */
    _setRegNumberInput(el) {
        const isRegistrationNumberRequired = this._getIsRegistrationNumberRequired(el);

        const modalContent = this.paymentModal.getModalContent();
        const regNumberInput = DomAccess.querySelector(modalContent, this.options.regNumberInputSelector, false);
        const regNumberRequired = DomAccess.querySelector(modalContent, this.options.regNumberRequiredSelector, false);

        if (isRegistrationNumberRequired) {
            regNumberInput.setAttribute('required', 'required');
            regNumberRequired.classList.remove('d-none');
        } else {
            regNumberInput.removeAttribute('required');
            regNumberRequired.classList.add('d-none');
        }
    }
}
