import LoadingIndicatorUtil from 'src/utility/loading-indicator/loading-indicator.util';

const ELEMENT_LOADER_CLASS = 'sprinque-ellipsis-loader';

export default class ElementLoadingIndicatorUtil extends LoadingIndicatorUtil {

    /**
     * adds the loader from the element
     *
     * @param {HTMLElement} el
     */
    static create(el) {
        el.classList.add('has-element-loader');
        if (ElementLoadingIndicatorUtil.exists(el)) return;
        ElementLoadingIndicatorUtil.prependLoader(el);
    }

    /**
     * removes the loader from the element
     *
     * @param {HTMLElement} el
     */
    static remove(el) {
        el.classList.remove('has-element-loader');
        const loader = el.querySelector(`.${ELEMENT_LOADER_CLASS}`);
        if (!loader) {
            return;
        }

        loader.remove();
    }

    /**
     * checks if a loader is already present
     *
     * @param {HTMLElement} el
     *
     * @returns {boolean}
     */
    static exists(el) {
        return (el.querySelectorAll(`.${ELEMENT_LOADER_CLASS}`).length > 0);
    }


    /**
     * returns the loader template
     *
     * @returns {string}
     */
    static getTemplate() {
        return `
            <div class="${ELEMENT_LOADER_CLASS}">
                <div></div><div></div><div></div><div></div>
            </div>
        `;
    }

    /**
     * inserts the loader into the passed element
     *
     * @param {HTMLElement} el
     */
    static prependLoader(el) {
        el.insertAdjacentHTML('afterbegin', ElementLoadingIndicatorUtil.getTemplate());
    }
}
