import { DATA_BAG_KEY } from './constants.util';

export const joinPath = (...paths) => {
    const pathData = paths.map(path => path.replace(/^\/|\/$/g, ''));

    return pathData.join('/');
};

export const createInput = (field, value) => {
    const input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', `${DATA_BAG_KEY}[${field}]`);
    input.setAttribute('value', value);

    return input;
};
