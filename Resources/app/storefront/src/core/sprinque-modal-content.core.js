import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';
import HttpClient from 'src/service/http-client.service';
import ElementLoadingIndicatorUtil from 'src/utility/loading-indicator/element-loading-indicator.util';
import AppClient from '../service/app-client.service';
import formState from '../store/form.state';
import paymentState from '../store/payment.state';
import { createInput } from '../util/helper.util';

export default class SprinqueModalContentCore extends Plugin {
    static options = {
        contentSelector: null,
        exception: {},
        baseUrl: null,
        shopId: null,
        appUrl: null,
        totalAmount: null,
        paymentFormId: null,
        modalFormAttribute: 'data-sprinque-form',
        errorAlertSelector: '#sprinque-alert-error',
        errorAlertContentSelector: '.alert-content',
        nextBtnSelector: '.js-sprinque-next-btn',
        prevBtnSelector: '.js-sprinque-prev-btn',
        submitOrderBtnSelector: '.js-sprinque-submit-order-btn',
        nextModalSelector: '',
        prevModalSelector: '',
        errorModalSelector: '#sprinque-error',
        doNotMissOutModalSelector: '#do-not-miss-out',
    };

    init() {
        const {
            shopId,
            appUrl,
            contentSelector,
            nextBtnSelector,
            prevBtnSelector,
            paymentFormId,
            submitOrderBtnSelector,
            errorAlertSelector,
        } = this.options;

        this.appClient = new AppClient(shopId, appUrl);
        this.httpClient = new HttpClient();
        this.paymentModal = window.PluginManager.getPluginInstances('SprinquePaymentModal')[0];
        this.paymentForm = DomAccess.querySelector(document, paymentFormId, false);
        this.errorAlertEl = DomAccess.querySelector(this.el, errorAlertSelector, false);
        this.contentEl = DomAccess.querySelector(this.el, contentSelector, false);
        this.modalForm = DomAccess.querySelector(this.contentEl, 'form', false);
        this.nextBtn = DomAccess.querySelector(this.contentEl, nextBtnSelector, false);
        this.prevBtn = DomAccess.querySelector(this.contentEl, prevBtnSelector, false);
        this.submitOrderBtnEl = DomAccess.querySelector(this.contentEl, submitOrderBtnSelector, false);

        // Prevent form submit on enter
        this.modalForm && this.modalForm.addEventListener('keypress', (e) => {
            if (e.keyCode === 13) {
                e.preventDefault();
            }
        });

        this._registerButtonEvents();
    }

    /**
     * Handle form data
     * @param additionalData
     */
    saveForm(additionalData = {}) {
        if (!this.modalForm) {
            return;
        }

        const formAttribute = this.modalForm.getAttribute(this.options.modalFormAttribute);
        if (!formAttribute) {
            return;
        }

        let data = {};
        for (const el of this.modalForm.elements) {
            if (!el.name) {
                continue;
            }

            data = {
                ...data,
                [el.name]: el.value,
                ...additionalData,
            };
        }

        formState.updateForm(formAttribute, data);
    }

    /**
     * Replace modal content with the error content
     * @param title
     * @param description
     */
    showErrorModal(title, description) {
        this.paymentModal.setContent(this.options.errorModalSelector, {
            title,
            description,
        });
    }

    /**
     * Replace modal content with the exception content
     */
    showException(e) {
        const { errors } = e || {};
        const errorMessages = Object.values(errors || {})[0] || [];
        if (Array.isArray(errorMessages) && errorMessages.length > 0) {
            this._displayAlertMessage(errorMessages[0]);

            return;
        }

        const {
            title,
            description,
        } = this.options.exception || {};

        this.showErrorModal(title, description);
    }

    _registerButtonEvents() {
        this.nextBtn && this.nextBtn.addEventListener('click', this._onNextBtnClick.bind(this));
        this.prevBtn && this.prevBtn.addEventListener('click', this._onPrevBtnClick.bind(this));
        this.submitOrderBtnEl && this.submitOrderBtnEl.addEventListener('click', this._onSubmitButtonClick.bind(this));
        this.paymentModal && this.paymentModal.$emitter.subscribe(
            'Sprinque/onModalClose',
            this._onModalClose.bind(this),
        );
    }

    _onNextBtnClick(e) {
        e.preventDefault();
        this._displayAlertMessage();
        this.saveForm();
        this.options.nextModalSelector && this.paymentModal.setContent(this.options.nextModalSelector);
    }

    _onPrevBtnClick() {
        this.options.prevModalSelector && this.paymentModal.setContent(this.options.prevModalSelector);
    }

    async _onSubmitButtonClick() {
        await this._submitPayment();
    }

    async _submitPayment(shouldPrepareOrder = false) {
        try {
            this.paymentModal.setContent(this.options.doNotMissOutModalSelector);
            ElementLoadingIndicatorUtil.create(this.el);

            const { orderId } = this.options;

            // If orderId exists, we add payment data to order
            // otherwise, we add payment data to form
            // (this is because storefront won't add storefront data to order)
            if (orderId || shouldPrepareOrder) {
                const appPaymentTermId = await this._prepareOrder(orderId);
                if (appPaymentTermId) {
                    this.paymentForm.append(createInput('appPaymentTermId', appPaymentTermId));
                }
            }

            // Create input for each payment state values and append to payment form
            for (const [field, value] of Object.entries(paymentState.getPayment())) {
                this.paymentForm.append(createInput(field, value));
            }

            this.paymentForm.submit();
        } finally {
            ElementLoadingIndicatorUtil.remove(this.el);
        }
    }

    _onModalClose() {
    }

    async _prepareOrder(orderId) {
        return this.appClient.post('/order/prepare', {
            orderId,
            ...paymentState.getPayment(),
        });
    }

    _displayAlertMessage(message = null) {
        const { errorAlertContentSelector } = this.options;
        if (message) {
            this.errorAlertEl.classList.remove('d-none');
            DomAccess.querySelector(this.errorAlertEl, errorAlertContentSelector).innerHTML = message;
        } else {
            this.errorAlertEl.classList.add('d-none');
            DomAccess.querySelector(this.errorAlertEl, errorAlertContentSelector).innerHTML = '';
        }
    }
}
