import SprinquePaymentModal from './sprinque/sprinque-payment-modal.plugin';
import SprinqueCountrySelect from './sprinque/sprinque-country-select.plugin';
import SprinqueRegistrationNumberValidation from './sprinque/sprinque-registration-number-validation.plugin';

import SprinqueError from './sprinque/modal-content/sprinque-error';
import SprinqueSelectBusiness from './sprinque/modal-content/sprinque-select-business.plugin';
import SprinqueCompanyAddress from './sprinque/modal-content/sprinque-company-address.plugin';
import SprinqueThanksForYourPatience from './sprinque/modal-content/sprinque-thanks-for-your-patience';
import SprinqueVerifyEmail from './sprinque/modal-content/sprinque-verify-email.plugin';
import SprinqueManualReview from './sprinque/modal-content/sprinque-manual-review';
import SprinquePaymentTerm from './sprinque/modal-content/sprinque-payment-term';
import SprinqueDoNotMissOut from './sprinque/modal-content/sprinque-do-not-miss-out.plugin';

const PluginManager = window.PluginManager;

PluginManager.register('SprinquePaymentModal', SprinquePaymentModal, '[data-sprinque-payment-modal]');
PluginManager.register('SprinqueCountrySelect', SprinqueCountrySelect, '[data-sprinque-country-select]');
PluginManager.register('SprinqueRegistrationNumberValidation', SprinqueRegistrationNumberValidation, '[data-sprinque-registration-number-validation]');

PluginManager.register('SprinqueError', SprinqueError, '[data-sprinque-error]');
PluginManager.register('SprinqueSelectBusiness', SprinqueSelectBusiness, '[data-sprinque-select-business]');
PluginManager.register('SprinqueCompanyAddress', SprinqueCompanyAddress, '[data-sprinque-company-address]');
PluginManager.register('SprinqueThanksForYourPatience', SprinqueThanksForYourPatience, '[data-sprinque-thanks-for-your-patience]');
PluginManager.register('SprinqueVerifyEmail', SprinqueVerifyEmail, '[data-sprinque-verify-email]');
PluginManager.register('SprinqueManualReview', SprinqueManualReview, '[data-sprinque-manual-review]');
PluginManager.register('SprinqueDoNotMissOut', SprinqueDoNotMissOut, '[data-sprinque-do-not-miss-out]');
PluginManager.register('SprinquePaymentTerm', SprinquePaymentTerm, '[data-sprinque-payment-term]');
