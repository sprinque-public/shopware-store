<p align="center">
  <img src=".github/assets/sprinque-logo.png" alt="Checkout.com logo" width="300" />
</p>

# Sprinque Shopware payment app
[Sprinque](https://www.sprinque.com "Sprinque") is the B2B payments platform that helps you expand globally.

This is the official Sprinque payment app for Shopware 6. It allows shop owners to offer `Pay by Invoice` with Net Payment Terms regardless of where they are:
- Get paid upfront, eliminate all risk and say goodbye to chasing late payments. We do the heavy lifting.
- Turn payments into a strategic asset for you to focus on what you do best: Growing your business.

## Compatibility
The app is compatible with Shopware 6.5.0.0 and later.

## Manual installation
Please refer to this [guide](../../wiki/Home.md) to install the app manually.

## Usage
For more information about the plugin configuration, please refer to our [wiki page](../../wikis).

## Communicate between Shopware and Apps

You can communicate between Shopware and Apps via `AppClient`.
For a more information on concept refer to the [according ADR](adr/2023-08-26-setup-api-to-communicate-between-shopware-and-app.md).

