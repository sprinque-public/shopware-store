module.exports = {
    env: {
        browser: true,
        node: true,
    },
    plugins: [
        'cypress',
    ],
    extends: 'plugin:cypress/recommended',
    overrides: [
    ],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module'
    },
    rules: {
        semi: ['error', 'never'],
        quotes: ['error', 'single'],
    }
}
