var path = require('path')
const webpack = require('webpack')

module.exports = {
    resolve: {
        alias: {
            '@actions': path.resolve(__dirname, 'cypress/support/actions'),
            '@services': path.resolve(__dirname, 'cypress/support/services'),
            '@repositories': path.resolve(__dirname, 'cypress/support/repositories'),
            '@scenarios': path.resolve(__dirname, 'cypress/support/scenarios'),
        },
        fallback: {
            'path': require.resolve('path-browserify'),
            'fs': false
        }
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.APP_URL': JSON.stringify('http://sprinque-shop.test'),
        })
    ]
}
