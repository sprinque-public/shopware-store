### Test environment & DB
Follow these steps to set up the test environment if you run E2E for the first time:

On you Shopware installation directory, go to your `.env` file and change the DB name to your test DB:
```ruby 
DATABASE_URL=mysql://app:app@localhost:3306/sprinque_shop_e2e
```

On your Shopware installation directory, go to your `.env` file and change the APP_ENV to `e2e`:
```ruby
APP_ENV="e2e"
```

On Shopware production template, run:
```ruby
bin/console system:install --drop-database --create-database --basic-setup --force
```

Or this command on Shopware platform:
```ruby
composer e2e:setup
```

At this point, we have a clean Shopware installation on local for running E2E tests. But to actually install the App, we would need to set the secret key to the app

```xml
<setup>
    <secret>YOUR_SECRET_KEY</secret> <!-- replace app secret from your Shopware instance -->
</setup>
```

After this, you should go into our App's `env` and set `SW_APP_SECRET` with the newly created secret key.

Now you can be able to install the App:

```ruby
bin/console app:install Sprinque --activate
bin/console e2e:dump-db
```

And if you are done with testing, to switch back to dev DB for development, go to your `.env` file and change your DB:
```ruby 
DATABASE_URL=mysql://app:app@localhost:3306/sprinque_shop
```

### Installation
Go to `/custom/apps/Sprinque/tests/e2e` and run:
```ruby 
npm install
```
After installing, you need to set all the necessary environment variables for Cypress to work. We have provided `cypress.config.example.js` to define all the environment variables our plugin needs in order to run.

Create `cypress.config.js` inside `Sprinque/tests/e2e` directory. In here, you will need to set `baseUrl` and `apiKey` to your current testing Shopware instance.

### Cypress UI
```ruby 
make open-ui shopware=6.5.4.1 url=https://my-local-or-remote-domain
```

### Run in CLI
You can also use the CLI command to run Cypress on your machine or directly in your build pipeline.

```ruby 
make run shopware=6.5.4.1 url=https://my-local-or-remote-domain
```
