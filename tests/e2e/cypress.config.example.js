const { defineConfig } = require('cypress')

module.exports = defineConfig({
    defaultCommandTimeout: 10000,
    chromeWebSecurity: false,
    env: {
        user: 'admin',
        pass: 'shopware',
        salesChannelName: 'Storefront',
        admin: '/admin',
        apiPath: 'api',
        shopwareRoot: '../../../../..',
        localUsage: true,
        apiKey: '',
        MAILOSAUR_API_KEY: '',
    },
    e2e: {
        baseUrl: '',
        testIsolation: true,
        experimentalWebKitSupport: true,
        setupNodeEvents(on, config) {
            return require('./cypress/plugins/index.js')(on, config)
        },
    },
})
