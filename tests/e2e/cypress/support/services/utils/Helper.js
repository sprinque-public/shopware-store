export const generateRandomEmail = () => {
    const domain = 'xdxhrsxw.mailosaur.net'
    const randomString = new Date().getTime()

    return `shopware+${randomString}@${domain}`
}

/**
 * Generate random code for OTP
 * @param givenNumber
 * @returns {number}
 */
export const generateRandomCode = (givenNumber) => {
    let randomNum

    // Keep generating random numbers until we get a different one.
    do {
        randomNum = Math.floor(Math.random() * 90000) + 10000 // Generates a random 5-digit number
    } while (randomNum === givenNumber)

    return randomNum
}
