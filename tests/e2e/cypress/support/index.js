Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test because some third party apps
    // cause an error in the console which stops the test
    return false
})
