import searchBusinessRepository from '@repositories/storefront/sprinque/SearchBusinessRepository'
import verifyEmailRepository from '@repositories/storefront/sprinque/VerifyEmailRepository'

class SprinqueAction {
    /**
     * Select first business from the list
     */
    selectFirstBusiness() {
        searchBusinessRepository.getBusinessList().should('exist')
        searchBusinessRepository.getBusinessItem().should('have.length.gt', 1)
        searchBusinessRepository.getBusinessItem().eq(1).click()
        searchBusinessRepository.getSelectedBusinessCard().should('exist')
    }

    /**
     * Input business name and search
     * @param name
     */
    searchBusiness(name) {
        searchBusinessRepository.getSearchInput().clear().type(name)

        cy.intercept('POST', '/sw/sprinque/storefront-api/search/business').as('searchBusiness')
        cy.intercept('POST', '/storefront/script/sprinque-business-search').as('getResult')
        cy.wait('@searchBusiness').wait('@getResult')
    }

    /**
     * Get OTP from email and verify
     * @param subject
     * @param html
     */
    getOTPAndVerify({ subject, html }) {
        expect(subject).to.equal('Email verification')
        const code = html.codes[0].value
        verifyEmailRepository.getOtpInput().first().type(code)
        verifyEmailRepository.getNextButton().should('not.be.disabled').click()

        cy.intercept('POST', 'sw/sprinque/storefront-api/buyer/detail').as('buyerDetail')
        cy.intercept('POST', 'sw/sprinque/storefront-api/buyer/verify-otp').as('verifyOTP')
        cy.wait('@verifyOTP').wait('@buyerDetail')
    }
}

export default new SprinqueAction()
