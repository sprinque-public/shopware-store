import listingRepository from '@repositories/storefront/ListingRepository'
import offCanvasRepository from '@repositories/storefront/OffCanvasRepository'
import checkoutConfirmRepository from '@repositories/storefront/CheckoutConfirmRepository'

class CheckoutAction {

    /**
     * Find first product from listing and add to cart with a specific quantity
     * @param quantity
     */
    addFirstProductToCart(quantity) {
        const product = listingRepository.getFirstProduct()
        product.find('.btn-buy').click()

        if (quantity && quantity > 1) {
            offCanvasRepository.getQuantitySelect().type(`${quantity}`)
        }
    }

    /**
     * Click checkout button from offcanvas
     */
    checkoutFromOffcanvas() {
        offCanvasRepository.getCheckoutButton().click()
    }

    /**
     * select payment method by method name
     * @param paymentName
     */
    selectPaymentMethod(paymentName) {
        cy.get('.payment-methods').contains(paymentName).click({force: true})
    }

    /**
     * Redirect to order management screen
     */
    goToOrderScreen() {
        cy.get('.header-logo-main-link').click()
        cy.visit('/account/order')
    }

    /**
     * Check for terms and conditions
     */
    checkTermAndCondition() {
        checkoutConfirmRepository.getTermAndConditionCheckbox().click(1, 1)
    }
}

export default new CheckoutAction()
