import AdminAPIClient from '@services/shopware/AdminAPIClient'
import {generateRandomCode} from '@services/utils/Helper'

class ShopConfigurationAction {
    constructor() {
        this.apiClient = AdminAPIClient
    }


    /**
     * Setup necessary data for the plugin and clear cache
     */
    async setupShop() {
        await this.setupPlugin()
        this._clearCache()
    }

    /**
     * Open app configuration page and click "Save"
     */
    saveAppConfig() {
        cy.openInitialPage(`${Cypress.env('admin')}#/sw/extension/my-extensions/listing/`)

        cy.get('.sw-skeleton').should('not.exist')
        cy.get('.sw-loader').should('not.exist')

        cy.getSDKiFrame('sw-main-hidden')
            .should('exist')

        cy.get('.sw-meteor-page__smart-bar-title')
            .should('be.visible')
        cy.contains('.sw-meteor-page__smart-bar-title', 'My extensions')
        cy.get('.sw-skeleton')
            .should('not.exist')
        cy.get('.sw-loader')
            .should('not.exist')

        cy.contains('.sw-meteor-card__content-wrapper', 'Shopware Sprinque').within(() => {
            cy.get('.sw-context-button > .sw-context-button__button')
                .click()
        })

        cy.get('.sw-context-menu__content').contains('Open extension')
            .click()

        cy.contains('.smart-bar__content', 'Sprinque')

        cy.getSDKiFrame('sq-main-module').find('.sprinque-main-module').should('exist')

        cy.intercept('POST', '/sw/sprinque/admin-api/configs').as('getConfig')

        cy.wait('@getConfig')

        cy.getSDKiFrame('sq-main-module').find('.sw-button').contains('Save').click()
    }


    /**
     * Set default data for the app to work with all sales channels
     */
    async setupPlugin() {
        // assign all payment methods to
        // all available sales channels
        this.apiClient.get('/sales-channel')
            .then(channels => {

                if (channels === undefined || channels === null) {
                    throw new Error('Attention, No Sales Channels found from Shopware API')
                }

                channels.forEach(async channel => {
                    await this._activatePaymentMethods(channel.id)
                })
            })
        await this._configSprinque()
    }

    /**
     * Configure Sprinque app
     * @private
     */
    _configSprinque() {
        this.apiClient.post('/_action/system-config', {
            'Sprinque.config.apiKey': Cypress.env('apiKey'),
            'Sprinque.config.testMode': true,
            'Sprinque.config.sprinqueStyling': true
        })
    }

    _updateSrpinqueConfig(payload) {
        this.apiClient.post('/_action/system-config', payload)
    }

    /**
     * Activate Sprinque payment methods for all sales channels
     * @param id
     * @private
     */
    _activatePaymentMethods(id) {
        this.apiClient.get('/payment-method')
            .then(payments => {

                if (payments === undefined || payments === null) {
                    throw new Error('Attention, No payments from Shopware API')
                }

                let paymentMethodsIds = []

                payments.forEach(element => {
                    paymentMethodsIds.push({
                        'id': element.id,
                    })
                })

                const data = {
                    'id': id,
                    'paymentMethods': paymentMethodsIds,
                }

                this.apiClient.patch('/sales-channel/' + id, data)
            })
    }

    /**
     * Clear cache
     * @returns {*}
     * @private
     */
    _clearCache() {
        return this.apiClient.delete('/_action/cache')
            .catch((err) => {
                console.log('Cache could not be cleared')
            })
    }

    setNumberRange() {
        this.apiClient.get('/number-range').then((result) => {
            result.forEach(({ attributes: { name }, id }) => {
                const code = generateRandomCode(300)
                if (name === 'Invoices') {
                    this.apiClient.patch('/number-range/' + id, {
                        id,
                        pattern: `SI-652-${code}-{n}`
                    })
                }

                if (name === 'Credit notes') {
                    this.apiClient.patch('/number-range/' + id, {
                        id,
                        pattern: `SC-652-${code}-{n}`
                    })
                }
            })
        })
    }

}

export default new ShopConfigurationAction()
