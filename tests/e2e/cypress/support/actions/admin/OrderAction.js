class OrderAction {
    /**
     * Click on first order on order listing page
     */
    clickFirstOrder() {
        cy.intercept({
            url: `**/${Cypress.env('apiPath')}/search/order`,
            method: 'POST',
        }).as('orderCall')

        cy.intercept({
            url: `**/${Cypress.env('apiPath')}/search/state-machine-state`,
            method: 'post',
        }).as('stateMachineTypeCall')

        // wait for state select to load all available state machines
        cy.intercept({
            url: `**/${Cypress.env('apiPath')}/_action/state-machine/**/**/state`,
            method: 'get',
        }).as('stateMachineTypeStateCall')

        cy.clickContextMenuItem(
            '.sw-order-list__order-view-action',
            '.sw-context-button__button',
            '.sw-data-grid__row--0'
        )

        cy.get('.sw-skeleton.sw-skeleton__detail').should('not.exist')

        cy.wait('@orderCall').its('response.statusCode').should('equal', 200)
        cy.wait('@stateMachineTypeCall').its('response.statusCode').should('equal', 200)
        cy.wait('@stateMachineTypeStateCall').its('response.statusCode').should('equal', 200)
    }
}

export default new OrderAction()
