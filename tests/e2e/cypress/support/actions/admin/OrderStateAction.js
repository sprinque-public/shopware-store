class OrderStateAction {
    /**
     * Change state of order
     * @param state
     * @param input
     * @param value
     */
    changeState(state, input, value) {
        const stateSelector = this._getStatesSelector(state)

        // Request we want to wait for later

        const callType = state === 'order' ? '' : `order_${state}`
        cy.intercept({
            url: `**/${Cypress.env('apiPath')}/_action/${callType}/**/state/${value}`,
            method: 'post',
        }).as('changeState')

        cy.get('.sw-loader').should('not.exist')
        cy.get(`${stateSelector} .sw-loader__element`).should('not.exist')
        cy.get(stateSelector).scrollIntoView()

        cy.get(stateSelector)
            .should('be.visible')
            .typeSingleSelect(
                input,
                stateSelector
            )

        cy.get('.sw-skeleton').should('not.exist')
        cy.get('.sw-loader').should('not.exist')

        cy.get('.sw-order-state-change-modal').should( 'be.visible')

        cy.get('.sw-order-state-change-modal-attach-documents__button').click()

        cy.wait('@changeState').its('response.statusCode').should('equal', 200)
    }

    _getStatesSelector(type) {
        const stateSelects = {
            orderTransactionStateSelect: '.sw-order-state-select-v2__order_transaction',
            orderDeliveryStateSelect: '.sw-order-state-select-v2__order_delivery',
            orderStateSelect: '.sw-order-state-select-v2__order',
        }

        switch (type) {
            case 'payment':
                return stateSelects.orderTransactionStateSelect
            case 'delivery':
                return stateSelects.orderDeliveryStateSelect
            case 'order':
                return stateSelects.orderStateSelect
            default:
                console.error(`Unknown state type ${type}`)
        }
    }
}

export default new OrderStateAction()
