class CheckoutConfirmRepository {

    /**
     * get term and condition checkbox
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getTermAndConditionCheckbox() {
        return cy.get('#tos')
    }

    /**
     * get confirm submit button
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getConfirmSubmitButton() {
        return cy.get('#confirmFormSubmit').scrollIntoView()
    }


    /**
     * get payment method
     * @param name
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getPaymentMethod(name) {
        return cy.get('.payment-methods').contains(name)
    }
}

export default new CheckoutConfirmRepository()
