class SearchBusinessRepository {

    /**
     * Get modal content
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getModalContent() {
        return cy.get('#search-business')
    }

    /**
     * Get country select
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getCountrySelect() {
        return this.getModalContent().get('#country')
    }

    /**
     * Get registration number input
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getRegistrationNumber() {
        return this.getModalContent().find('#registration_number')
    }

    /**
     * Get vat id input
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getVatId() {
        return this.getModalContent().find('#type_vat_id')
    }

    /**
     * Get search type
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getSearchType() {
        return this.getModalContent().find('.sprinque-company-type')
    }

    /**
     * Get search input
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getSearchInput() {
        return this.getModalContent().find('#sprinque-search-business')
    }

    /**
     * Get business list
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getBusinessList() {
        return this.getModalContent().find('.sprinque-business-list')
    }

    /**
     * Get business item
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getBusinessItem() {
        return this.getModalContent().find('.sprinque-business-list-item')
    }

    /**
     * Get selected business card
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getSelectedBusinessCard() {
        return this.getModalContent().find('.sprinque-selected-business-card')
    }

    /**
     * Get "Next" button
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getNextButton() {
        return this.getModalContent().find('.js-sprinque-next-btn')
    }
}

export default new SearchBusinessRepository()
