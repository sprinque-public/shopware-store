class PaymentTermsRepository {
    getModalContent() {
        return cy.get('#payment-terms')
    }

    /**
     * Get "Submit" button
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getSubmitButton() {
        return this.getModalContent().find('.js-sprinque-submit-order-btn')
    }

    /**
     * Get payment terms radio buttons
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getPaymentTermsRadio() {
        return this.getModalContent().find('.term-select')
    }
}

export default new PaymentTermsRepository()
