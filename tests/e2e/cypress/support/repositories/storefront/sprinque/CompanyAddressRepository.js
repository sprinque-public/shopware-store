class CompanyAddressRepository {
    getModalContent() {
        return cy.get('#company-address')
    }

    /**
     * Get "Next" button
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getNextButton() {
        return this.getModalContent().find('.js-sprinque-next-btn')
    }

    /**
     * Get "Address" input
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getAddressInput() {
        return this.getModalContent().find('#address_line_1')
    }

    /**
     * Get "City" input
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getCityInput() {
        return this.getModalContent().find('#city')
    }

    /**
     * Get "Zip code" input
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getZipCodeInput() {
        return this.getModalContent().find('#zip_code')
    }
}

export default new CompanyAddressRepository()
