class ErrorRepository {
    getModalContent() {
        return cy.get('#sprinque-error')
    }
}

export default new ErrorRepository()
