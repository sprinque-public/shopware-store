class VerifyEmailRepository {
    /**
     * Get modal content
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getModalContent() {
        return cy.get('#verify-email')
    }

    /**
     * Get OTP input
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getOtpInput() {
        return this.getModalContent().find('.otp-input')
    }

    /**
     * get "Next" button
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getNextButton() {
        return this.getModalContent().find('.js-sprinque-next-btn')
    }

    /**
     * Get OTP container
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getOTPContainer() {
        return this.getModalContent().find('#otp-input')
    }

    /**
     * Get "Resend" button
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getResendButton() {
        return this.getModalContent().find('#resend-btn')
    }
}

export default new VerifyEmailRepository()
