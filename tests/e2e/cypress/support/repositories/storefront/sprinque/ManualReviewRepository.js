class ManualReviewRepository {
    getModalContent() {
        return cy.get('#manual-review')
    }

    /**
     * Get "Continue and wait" button
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getSubmitButton() {
        return this.getModalContent().find('.js-sprinque-submit-order-btn')
    }

    /**
     * Get "Use another payment method" button
     * @returns {Cypress.Chainable<JQuery<HTMLElement>>}
     */
    getUseAnotherMethodButton() {
        return this.getModalContent().find('.js-sprinque-close-btn')
    }
}

export default new ManualReviewRepository()
