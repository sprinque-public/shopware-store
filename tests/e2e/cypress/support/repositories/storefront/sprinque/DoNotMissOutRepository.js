class DoNotMissOutRepository {
    getModalContent() {
        return cy.get('#do-not-miss-out')
    }
}

export default new DoNotMissOutRepository()
