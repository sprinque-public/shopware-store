const { v4: uuid } = require('uuid')

/**
 * checks iframe content for sdk test
 * @memberOf Cypress.Chainable#
 * @name getSDKiFrame
 * @param {strong} iframe - String of custom url to select iframe
 * @function
 */
Cypress.Commands.add('getSDKiFrame', (locationId) => {
    cy.get(`iframe[src*="location-id=${locationId}"]`)
        .its('0.contentDocument.body')
        .should('not.be.empty')
        .then(cy.wrap)
})

/**
 * @memberOf Cypress.Chainable#
 * @name SqCreateCustomer
 * @function
 */
Cypress.Commands.add('SqCreateCustomer', (userData, iso = 'DE') => {
    const addressId = uuid().replace(/-/g, '')
    const customerId = uuid().replace(/-/g, '')
    let customerJson = {}
    let customerAddressJson = {}
    let finalAddressRawData = {}
    let countryId = ''
    let groupId = ''
    let paymentId = ''
    let salesChannelId = ''
    let salutationId = ''

    return cy.fixture('customer').then((result) => {
        customerJson = Cypress._.merge(result, userData)

        return cy.fixture('customer-address')
    }).then((result) => {
        customerAddressJson = result

        return cy.searchViaAdminApi({
            endpoint: 'country',
            data: {
                field: 'iso',
                value: iso
            }
        })
    }).then((result) => {
        countryId = result.id

        return cy.searchViaAdminApi({
            endpoint: 'payment-method',
            data: {
                field: 'name',
                value: 'Invoice'
            }
        })
    }).then((result) => {
        paymentId = result.id

        return cy.searchViaAdminApi({
            endpoint: 'sales-channel',
            data: {
                field: 'name',
                value: 'Storefront'
            }
        })
    }).then((result) => {
        salesChannelId = result.id

        return cy.searchViaAdminApi({
            endpoint: 'customer-group',
            data: {
                field: 'name',
                value: 'Standard customer group'
            }
        })
    }).then((result) => {
        groupId = result.id

        return cy.searchViaAdminApi({
            endpoint: 'salutation',
            data: {
                field: 'displayName',
                value: 'Mr.'
            }
        })
    }).then((salutation) => {
        salutationId = salutation.id

        finalAddressRawData = Cypress._.merge({
            addresses: [{
                customerId: customerId,
                salutationId: salutationId,
                id: addressId,
                countryId: countryId
            }]
        }, customerAddressJson)
    }).then(() => {
        return Cypress._.merge(customerJson, {
            salutationId: salutationId,
            defaultPaymentMethodId: paymentId,
            salesChannelId: salesChannelId,
            groupId: groupId,
            defaultBillingAddressId: addressId,
            defaultShippingAddressId: addressId
        })
    }).then((result) => {
        return Cypress._.merge(result, finalAddressRawData)
    }).then((result) => {
        return cy.requestAdminApiStorefront({
            endpoint: 'customer',
            data: result
        })
    })
})
