class AdminFlowBuilderScenario {

    /**
     * Create dummy capture flow
     */
    createCaptureFlow() {
        this._createFlow(() => {
            cy.get('#sw-field--flow-name').type('Capture when order is in progress')
            cy.get('#sw-field--flow-priority').type('10')
            cy.get('.sw-flow-detail-general__general-active .sw-field--switch__input').click()

            cy.get('.sw-flow-detail__tab-flow').click()
            cy.get('.sw-flow-trigger__input-field').type('order state in progress')
            cy.get('.sw-flow-trigger__search-result').should('be.visible')
            cy.get('.sw-flow-trigger__search-result').eq(0).click()

            cy.get('.sw-flow-sequence-selector').should('be.visible')
            cy.get('.sw-flow-sequence-selector__add-action').click()
            cy.get('.sw-flow-sequence-action__selection-action')
                .typeSingleSelect('Sprinque capture', '.sw-flow-sequence-action__selection-action')
        })
    }

    /**
     * Create dummy refund flow
     */
    createRefundFlow() {
        this._createFlow(() => {
            cy.get('#sw-field--flow-name').type('Refund when order is cancelled')
            cy.get('#sw-field--flow-priority').type('10')
            cy.get('.sw-flow-detail-general__general-active .sw-field--switch__input').click()

            cy.get('.sw-flow-detail__tab-flow').click()
            cy.get('.sw-flow-trigger__input-field').type('order state cancelled')
            cy.get('.sw-flow-trigger__search-result').should('be.visible')
            cy.get('.sw-flow-trigger__search-result').eq(0).click()

            cy.get('.sw-flow-sequence-selector').should('be.visible')
            cy.get('.sw-flow-sequence-selector__add-action').click()
            cy.get('.sw-flow-sequence-action__selection-action')
                .typeSingleSelect('Sprinque refund', '.sw-flow-sequence-action__selection-action')
        })
    }

    /**
     * Create flow scenario
     * @param callback
     * @private
     */
    _createFlow(callback) {
        cy.authenticate().then(() => {
            cy.openInitialPage(`${Cypress.env('admin')}#/sw/flow/index`)
        })
        cy.get('.sw-skeleton').should('not.exist')
        cy.get('.sw-loader').should('not.exist')

        cy.intercept({
            url: `${Cypress.env('apiPath')}/flow`,
            method: 'POST',
        }).as('saveData')

        cy.get('.sw-flow-list').should('be.visible')
        cy.get('.sw-flow-list__create').click()

        // Verify "create" page
        cy.contains('.smart-bar__header h2', 'New flow')

        callback()

        cy.get('.sw-flow-app-action-modal__save-button').click()
        cy.get('.sw-flow-app-action-modal').should('not.exist')

        // Save
        cy.get('.sw-flow-detail__save').click()
        cy.wait('@saveData').its('response.statusCode').should('equal', 204)
    }
}

export default new AdminFlowBuilderScenario()
