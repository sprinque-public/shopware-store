import storefrontLoginAction from '@actions/storefront/LoginAction'
import checkoutAction from '@actions/storefront/CheckoutAction'
import checkoutConfirmRepository from '@repositories/storefront/CheckoutConfirmRepository'
import sprinqueAction from '@actions/storefront/SprinqueAction'
import searchBusinessRepository from '@repositories/storefront/sprinque/SearchBusinessRepository'
import companyAddressRepository from '@repositories/storefront/sprinque/CompanyAddressRepository'

class DummyCheckoutScenario {

    execute(email = 'test@example.com', password = 'shopware') {
        storefrontLoginAction.login(email, password)

        checkoutAction.addFirstProductToCart(1)

        checkoutAction.checkoutFromOffcanvas()

        checkoutAction.checkTermAndCondition()
    }

    submitOrder(email, password) {
        this.execute(email, password)

        checkoutAction.selectPaymentMethod('Pay by invoice by Sprinque')

        checkoutConfirmRepository.getConfirmSubmitButton().should('not.be.disabled').click()

        cy.intercept('POST', '/sw/sprinque/storefront-api/prepare-payment').as('preparePayment')
        cy.intercept('POST', '/storefront/script/sprinque-modal').as('openModal')

        cy.wait('@preparePayment').wait('@openModal')
    }

    /**
     * Select business and proceed to OTP verification
     */
    selectBusinessAndVerify() {
        this._selectBusiness()
        this._proceedToOtpVerification()
    }

    /**
     * Approve buyer instantly
     */
    approveBuyerInstantly() {
        this._selectBusiness()
        searchBusinessRepository.getRegistrationNumber().type('{home}APPROVE-')
        cy.wait(1000)
        this._proceedToOtpVerification()
    }

    /**
     * Reject buyer instantly
     */
    rejectBuyerInstantly() {
        this._selectBusiness()
        searchBusinessRepository.getRegistrationNumber().type('{home}REJECT-')
        cy.wait(1000)
        this._proceedToOtpVerification()
    }

    /**
     * search and select first business from the result
     * @private
     */
    _selectBusiness() {
        sprinqueAction.searchBusiness('ABC GmbH')
        sprinqueAction.selectFirstBusiness()
    }

    /**
     * Proceed to OTP verification
     * @private
     */
    _proceedToOtpVerification() {
        searchBusinessRepository.getNextButton().click()
        companyAddressRepository.getModalContent().should('exist')
        companyAddressRepository.getNextButton().click()

        cy.intercept('POST', '/sw/sprinque/storefront-api/buyer/register').as('registerBuyer')
        cy.intercept('POST', 'sw/sprinque/storefront-api/buyer/send-otp').as('sendOTP')
        cy.wait('@registerBuyer').wait('@sendOTP')
    }

    /**
     * Enter business detail manually and proceed to OTP verification
     */
    enterBusinessDetail() {
        sprinqueAction.searchBusiness('ABC GmbH')
        searchBusinessRepository.getBusinessItem().first().click()
        searchBusinessRepository.getRegistrationNumber().type('{home}APPROVE-')
        searchBusinessRepository.getNextButton().click()

        companyAddressRepository.getModalContent().should('exist')
        companyAddressRepository.getAddressInput().type('Test address')
        companyAddressRepository.getCityInput().type('Hamburg')
        companyAddressRepository.getZipCodeInput().type('1234')
        companyAddressRepository.getNextButton().click()

        cy.intercept('POST', '/sw/sprinque/storefront-api/buyer/register').as('registerBuyer')
        cy.intercept('POST', 'sw/sprinque/storefront-api/buyer/send-otp').as('sendOTP')
        cy.wait('@registerBuyer').wait('@sendOTP')
    }
}

export default new DummyCheckoutScenario()
