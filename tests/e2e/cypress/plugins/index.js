const webpack = require('@cypress/webpack-preprocessor')

module.exports = (on, config) => {
    on('file:preprocessor', webpack({
        webpackOptions: require('../../webpack.config'),
        watchOptions: {},
    }))

    on('before:browser:launch', (browser = {}, launchOptions) => {
        if (browser.name === 'chrome' || browser.name === 'edge') {
            launchOptions.args.push('--disable-features=SameSiteByDefaultCookies')
            return launchOptions
        }
    })

    return config
}
