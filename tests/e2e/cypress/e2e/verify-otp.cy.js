import shopConfigurationAction from '@actions/admin/ShopConfigurationAction'
import DummyCheckoutScenario from '@scenarios/DummyCheckoutScenario'
import {generateRandomCode, generateRandomEmail} from '@services/utils/Helper'
import verifyEmailRepository from '@repositories/storefront/sprinque/VerifyEmailRepository'

describe('Testing "Verify OTP" modal', ()=> {
    let email = null
    before(() => {
        // Set the Shopware instance to initial state only on local environment
        const promiseChain = Cypress.env('localUsage') ? cy.authenticate() : cy

        promiseChain.then(() => {
            return cy.setToInitialState()
        }).then(() => {
            return cy.authenticate()
        }).then(() => {
            return cy.createProductFixture()
        }).then(async () => {
            await shopConfigurationAction.setupShop()
            email = generateRandomEmail()
            cy.SqCreateCustomer({
                email
            })
        })
    })

    beforeEach(() => {
        DummyCheckoutScenario.submitOrder(email)
        DummyCheckoutScenario.selectBusinessAndVerify()
    })

    it('Enter incorrect OTP', () => {
        cy.mailosaurGetMessage('xdxhrsxw', {
            sentTo: email
        }).then(({ subject, html }) => {
            expect(subject).to.equal('Email verification')
            const code = generateRandomCode(html.codes[0].value)
            verifyEmailRepository.getOtpInput().first().type(code)
            verifyEmailRepository.getNextButton().should('not.be.disabled').click()

            cy.intercept('POST', 'sw/sprinque/storefront-api/buyer/verify-otp').as('verifyOTP')
            cy.wait('@verifyOTP')

            verifyEmailRepository.getOTPContainer().should('have.class', 'invalid')
        })
    })

    it('Resend OTP', () => {
        verifyEmailRepository.getModalContent().should('exist')
        verifyEmailRepository.getResendButton().should('be.disabled')

        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(31000)

        verifyEmailRepository.getResendButton().should('not.be.disabled').click()

        cy.wait('@sendOTP')
    })
})
