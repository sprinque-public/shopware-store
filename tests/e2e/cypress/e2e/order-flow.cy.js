import shopConfigurationAction from '@actions/admin/ShopConfigurationAction'
import DummyCheckoutScenario from '@scenarios/DummyCheckoutScenario'
import manualReviewRepository from '@repositories/storefront/sprinque/ManualReviewRepository'
import {generateRandomEmail} from '@services/utils/Helper'
import paymentTermsRepository from '@repositories/storefront/sprinque/PaymentTermsRepository'
import doNotMissOutRepository from '@repositories/storefront/sprinque/DoNotMissOutRepository'
import errorRepository from '@repositories/storefront/sprinque/ErrorRepository'
import sprinqueAction from '@actions/storefront/SprinqueAction'
import searchBusinessRepository from '@repositories/storefront/sprinque/SearchBusinessRepository'
import companyAddressRepository from '@repositories/storefront/sprinque/CompanyAddressRepository'

describe('Testing order flow', ()=> {
    let email = null
    let returningEmail = null

    before(() => {
        // Set the Shopware instance to initial state only on local environment
        const promiseChain = Cypress.env('localUsage') ? cy.authenticate() : cy

        promiseChain.then(() => {
            return cy.setToInitialState()
        }).then(() => {
            return cy.authenticate()
        }).then(() => {
            return cy.createProductFixture()
        }).then(async () => {
            await shopConfigurationAction.setupShop()
        })
    })

    beforeEach(() => {
        email = generateRandomEmail()
        cy.authenticate().then(() => {
            cy.SqCreateCustomer({
                email
            })
        })

        DummyCheckoutScenario.submitOrder(email)
    })

    it('Manual review: wait for a manual review', () => {
        DummyCheckoutScenario.selectBusinessAndVerify()

        cy.mailosaurGetMessage('xdxhrsxw', { sentTo: email })
            .then(sprinqueAction.getOTPAndVerify.bind(this))
            .then(() => {
            manualReviewRepository.getModalContent().should('exist')
            manualReviewRepository.getSubmitButton().should('exist').click()

            cy.url().should('include', '/checkout/finish')
        })
    })

    it('Manual review: Choose another payment method', () => {
        DummyCheckoutScenario.selectBusinessAndVerify()

        cy.mailosaurGetMessage('xdxhrsxw', { sentTo: email })
            .then(sprinqueAction.getOTPAndVerify.bind(this))
            .then(() => {
                manualReviewRepository.getModalContent().should('exist')
                manualReviewRepository.getUseAnotherMethodButton().should('exist').click()

                // Modal should be closed
                manualReviewRepository.getModalContent().should('not.exist')
            })
    })

    it('Approve buyer instantly', () => {
        if (!returningEmail) {
            returningEmail = email
        }

        DummyCheckoutScenario.approveBuyerInstantly()

        cy.mailosaurGetMessage('xdxhrsxw', { sentTo: email })
            .then(sprinqueAction.getOTPAndVerify.bind(this))
            .then(() => {
                paymentTermsRepository.getPaymentTermsRadio().first().click()
                paymentTermsRepository.getSubmitButton().should('not.be.disabled').click()

                doNotMissOutRepository.getModalContent().should('exist')
                cy.url().should('include', '/checkout/finish')
            })
    })

    it('Reject buyer instantly', () => {
        DummyCheckoutScenario.rejectBuyerInstantly()

        cy.mailosaurGetMessage('xdxhrsxw', { sentTo: email })
            .then(sprinqueAction.getOTPAndVerify.bind(this))
            .then(() => {
                errorRepository.getModalContent().should('exist')
            })
    })

    it('Returning buyer', { skipBeforeEach: true }, () => {
        cy.log('Email: ', returningEmail)
        if (!returningEmail) {
            it.skip('No returning buyer found')
        }

        cy.authenticate()
        DummyCheckoutScenario.submitOrder(returningEmail)

        searchBusinessRepository.getSearchInput().invoke('val').should('not.be.empty')
        searchBusinessRepository.getNextButton().should('not.be.disabled').click()
        companyAddressRepository.getNextButton().should('not.be.disabled').click()

        paymentTermsRepository.getPaymentTermsRadio().first().click()
        paymentTermsRepository.getSubmitButton().should('not.be.disabled').click()

        doNotMissOutRepository.getModalContent().should('exist')
        cy.url().should('include', '/checkout/finish')

    })

    it('Enter details manually', () => {
        DummyCheckoutScenario.enterBusinessDetail()

        cy.mailosaurGetMessage('xdxhrsxw', { sentTo: email })
            .then(sprinqueAction.getOTPAndVerify.bind(this))
            .then(() => {
                paymentTermsRepository.getPaymentTermsRadio().first().click()
                paymentTermsRepository.getSubmitButton().should('not.be.disabled').click()

                doNotMissOutRepository.getModalContent().should('exist')
                cy.url().should('include', '/checkout/finish')
            })
    })
})
