import shopConfigurationAction from '@actions/admin/ShopConfigurationAction'
import DummyCheckoutScenario from '@scenarios/DummyCheckoutScenario'
import searchBusinessRepository from '@repositories/storefront/sprinque/SearchBusinessRepository'

describe('Testing "Search your business" modal', ()=> {
    before(() => {
        // Set the Shopware instance to initial state only on local environment
        const promiseChain = Cypress.env('localUsage') ? cy.authenticate() : cy

        promiseChain.then(() => {
            return cy.setToInitialState()
        }).then(() => {
            return cy.authenticate()
        }).then(() => {
            return cy.createProductFixture()
        }).then(async () => {
            await shopConfigurationAction.setupShop()
            await cy.createCustomerFixtureStorefront()
        })
    })

    it('Check registration number required by selected countries', () => {
        DummyCheckoutScenario.submitOrder()

        // Registration should not be required by default
        searchBusinessRepository.getRegistrationNumber().should('not.have.attr', 'required')

        // Registration should be required when selected country is Netherlands
        searchBusinessRepository.getCountrySelect().select('NL')
        searchBusinessRepository.getRegistrationNumber().should('have.attr', 'required')
    })

    it('Check show/hide VAT option by selected countries', () => {
        DummyCheckoutScenario.submitOrder()

        // Search type select is available when selected country is Germany
        searchBusinessRepository.getSearchType().should('not.have.class', 'd-none')

        // Search type select should be hidden when selected country is Netherlands
        searchBusinessRepository.getCountrySelect().select('NL')
        searchBusinessRepository.getSearchType().should('have.class', 'd-none')
    })

    it('Search company by name', () => {
        DummyCheckoutScenario.submitOrder()

        searchBusinessRepository.getSearchInput().type('ABC GmbH')

        cy.intercept('POST', '/sw/sprinque/storefront-api/search/business').as('searchBusiness')
        cy.intercept('POST', '/storefront/script/sprinque-business-search').as('getResult')
        cy.wait('@searchBusiness').wait('@getResult')

        searchBusinessRepository.getBusinessList().should('exist')
    })

    it('Search company by VAT', () => {
        DummyCheckoutScenario.submitOrder()

        searchBusinessRepository.getVatId().click()

        searchBusinessRepository.getSearchInput().type('NL854502130B01')

        cy.intercept('POST', '/sw/sprinque/storefront-api/search/business').as('searchBusiness')
        cy.intercept('POST', '/storefront/script/sprinque-business-search').as('getResult')
        cy.wait('@searchBusiness').wait('@getResult')

        searchBusinessRepository.getBusinessList().should('exist')
    })
})
