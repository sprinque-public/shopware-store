import shopConfigurationAction from '@actions/admin/ShopConfigurationAction'
import DummyCheckoutScenario from '@scenarios/DummyCheckoutScenario'
import {generateRandomEmail} from '@services/utils/Helper'
import paymentTermsRepository from '@repositories/storefront/sprinque/PaymentTermsRepository'
import doNotMissOutRepository from '@repositories/storefront/sprinque/DoNotMissOutRepository'
import sprinqueAction from '@actions/storefront/SprinqueAction'
import shopware from '@services/shopware/Shopware'
import orderStateAction from '@actions/admin/OrderStateAction'
import orderAction from '@actions/admin/OrderAction'

describe('Testing order flow', ()=> {
    let email = null

    before(() => {
        // Set the Shopware instance to initial state only on local environment
        const promiseChain = Cypress.env('localUsage') ? cy.authenticate() : cy

        promiseChain.then(() => {
            return cy.setToInitialState()
        }).then(() => {
            return cy.authenticate()
        }).then(() => {
            return cy.createProductFixture()
        }).then(async () => {
            await shopConfigurationAction.setupShop()
            shopConfigurationAction.setNumberRange()

            email = generateRandomEmail()
            cy.authenticate().then(() => {
                cy.createCustomerFixtureStorefront({
                    email
                })
            })

            DummyCheckoutScenario.submitOrder(email)
            DummyCheckoutScenario.approveBuyerInstantly()

            cy.mailosaurGetMessage('xdxhrsxw', { sentTo: email })
                .then(sprinqueAction.getOTPAndVerify.bind(this))
                .then(() => {
                    paymentTermsRepository.getPaymentTermsRadio().first().click()
                    paymentTermsRepository.getSubmitButton().should('not.be.disabled').click()

                    doNotMissOutRepository.getModalContent().should('exist')
                    cy.url().should('include', '/checkout/finish')
                })
        })
    })

    it('Show Sprinque order detail card', () => {
        cy.skipOn(shopware.isVersionLower('6.5.5.1'))

        cy.openInitialPage(`${Cypress.env('admin')}#/sw/order/index`)

        cy.clickContextMenuItem(
            '.sw-order-list__order-view-action',
            '.sw-context-button__button',
            '.sw-data-grid__row--0'
        )

        // Check if order detail card exists
        cy.getSDKiFrame('sq-order-detail').find('.sq-order-detail').should('exist')
    })

    it('Capture an order', () => {
        cy.authenticate().then(() => {
            cy.openInitialPage(`${Cypress.env('admin')}#/sw/order/index`)
        })

        orderAction.clickFirstOrder()

        // Change delivery status to "Shipped"
        orderStateAction.changeState('delivery', 'Shipped', 'ship')
    })

    it('Partial refund', () => {
        cy.authenticate().then(() => {
            cy.openInitialPage(`${Cypress.env('admin')}#/sw/order/index`)
        })

        orderAction.clickFirstOrder()

        // Click on "Sprinque refund" button
        cy.get('.sw-app-actions__action-button').should('exist').click()
        cy.get('.sw-app-action-button').contains('Sprinque refund').click()

        // Type in refund amount and hit "Submit"
        cy.getSDKiFrame('sq-refund-modal').should('exist')
        cy.getSDKiFrame('sq-refund-modal').find('input').type(1).type(1)
        cy.get('.sw-modal .sw-button--primary').contains('Submit').click()

        cy.intercept('POST', '/sw/sprinque/admin-api/refund').as('refund')
        cy.wait('@refund').its('response.statusCode').should('equal', 204)

        // Check if order state is changed to "Refunded (partially)"
        cy.get('.sw-order-state-select-v2__order_transaction').contains('Refunded (partially)')
    })

    it('Full refund an order', () => {
        cy.authenticate().then(() => {
            cy.openInitialPage(`${Cypress.env('admin')}#/sw/order/index`)
        })

        orderAction.clickFirstOrder()

        // Change delivery status to "Cancelled"
        orderStateAction.changeState('order', 'Cancelled', 'cancel')
    })
})
