import shopConfigurationAction from '@actions/admin/ShopConfigurationAction'
import DummyCheckoutScenario from '@scenarios/DummyCheckoutScenario'
import checkoutConfirmRepository from '@repositories/storefront/CheckoutConfirmRepository'

describe('Check payment method visibility', ()=> {
    before(() => {
        // Set the Shopware instance to initial state only on local environment
        const promiseChain = Cypress.env('localUsage') ? cy.authenticate() : cy

        promiseChain.then(() => {
            return cy.setToInitialState()
        }).then(() => {
            return cy.authenticate()
        }).then(() => {
            return cy.createProductFixture()
        }).then(() => {
            return shopConfigurationAction.setupShop()
        }).then(() => {
            shopConfigurationAction.saveAppConfig()
        })
    })

    it('Show payment method if billing country is "Germany"', () => {
        cy.SqCreateCustomer()

        DummyCheckoutScenario.execute()

        checkoutConfirmRepository.getPaymentMethod('Pay by invoice by Sprinque').should('exist')
    })

    it('Hide payment method if billing country is "Turkey"', () => {
        // Create a customer with billing country "Turkey"
        cy.authenticate().then(() => {
            cy.SqCreateCustomer({
                email: 'test1@example.com'
            }, 'TR')
        })

        DummyCheckoutScenario.execute('test1@example.com')

        checkoutConfirmRepository.getPaymentMethod('Pay by invoice by Sprinque').should('not.exist')
    })
})
