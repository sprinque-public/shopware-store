import shopConfigurationAction from '@actions/admin/ShopConfigurationAction'
import DummyCheckoutScenario from '@scenarios/DummyCheckoutScenario'
import {generateRandomEmail} from '@services/utils/Helper'
import paymentTermsRepository from '@repositories/storefront/sprinque/PaymentTermsRepository'
import doNotMissOutRepository from '@repositories/storefront/sprinque/DoNotMissOutRepository'
import sprinqueAction from '@actions/storefront/SprinqueAction'
import shopware from '@services/shopware/Shopware'
import adminFlowBuilderScenario from '@scenarios/AdminFlowBuilderScenario'
import orderAction from '@actions/admin/OrderAction'
import orderStateAction from '@actions/admin/OrderStateAction'

describe('Testing Sprinque flow builder actions', ()=> {
    let email = null

    before(() => {
        // Set the Shopware instance to initial state only on local environment
        const promiseChain = Cypress.env('localUsage') ? cy.authenticate() : cy

        promiseChain.then(() => {
            return cy.setToInitialState()
        }).then(() => {
            return cy.authenticate()
        }).then(() => {
            return cy.createProductFixture()
        }).then(async () => {
            await shopConfigurationAction.setupShop()
            shopConfigurationAction.setNumberRange()

            email = generateRandomEmail()
            cy.authenticate().then(() => {
                cy.createCustomerFixtureStorefront({
                    email
                })
            })

            DummyCheckoutScenario.submitOrder(email)
            DummyCheckoutScenario.approveBuyerInstantly()

            cy.mailosaurGetMessage('xdxhrsxw', { sentTo: email })
                .then(sprinqueAction.getOTPAndVerify.bind(this))
                .then(() => {
                    paymentTermsRepository.getPaymentTermsRadio().first().click()
                    paymentTermsRepository.getSubmitButton().should('not.be.disabled').click()

                    doNotMissOutRepository.getModalContent().should('exist')
                    cy.url().should('include', '/checkout/finish')
                })
        })
    })

    it('Capture order', () => {
        cy.skipOn(shopware.isVersionLower('6.5.1.0'))

        adminFlowBuilderScenario.createCaptureFlow()

        cy.authenticate().then(() => {
            cy.openInitialPage(`${Cypress.env('admin')}#/sw/order/index`)
        })

        orderAction.clickFirstOrder()

        // Change order status to "In Progress"
        orderStateAction.changeState('order', 'In Progress', 'process')
    })

    it('Refund order', () => {
        cy.skipOn(shopware.isVersionLower('6.5.1.0'))

        adminFlowBuilderScenario.createRefundFlow()

        cy.authenticate().then(() => {
            cy.openInitialPage(`${Cypress.env('admin')}#/sw/order/index`)
        })

        orderAction.clickFirstOrder()

        // Change order status to "Cancelled"
        orderStateAction.changeState('order', 'Cancelled', 'cancel')
    })
})
