import shopConfigurationAction from '@actions/admin/ShopConfigurationAction'
import DummyCheckoutScenario from '@scenarios/DummyCheckoutScenario'
import checkoutAction from '@actions/storefront/CheckoutAction'

describe('App configuration', ()=> {
    before(() => {
        // Set the Shopware instance to initial state only on local environment
        const promiseChain = Cypress.env('localUsage') ? cy.authenticate() : cy

        promiseChain.then(() => {
            return cy.setToInitialState()
        }).then(() => {
            return cy.authenticate()
        }).then(() => {
            return cy.createProductFixture()
        }).then(async () => {
            await shopConfigurationAction.setupShop()
            await cy.createCustomerFixtureStorefront()
        })
    })

    it('Enable "Sprinque styling"', () => {
        shopConfigurationAction._updateSrpinqueConfig({
            'Sprinque.config.sprinqueStyling': true,
        })

        DummyCheckoutScenario.submitOrder()

        cy.get('.sprinque-styled-modal').should('exist')
    })

    it('Disable "Sprinque styling"', () => {
        shopConfigurationAction._updateSrpinqueConfig({
            'Sprinque.config.sprinqueStyling': false,
        })

        DummyCheckoutScenario.submitOrder()

        cy.get('.sprinque-unstyled-modal').should('exist')
    })

    it('Enable "User tracking"', () => {
        shopConfigurationAction._updateSrpinqueConfig({
            'Sprinque.config.trackingCode': true,
        })

        DummyCheckoutScenario.execute()
        checkoutAction.selectPaymentMethod('Pay by invoice by Sprinque')

        cy.window().should('have.property', '_fs_host', 'fullstory.com')

    })

    it('Disable "User tracking"', () => {
        shopConfigurationAction._updateSrpinqueConfig({
            'Sprinque.config.trackingCode': false,
        })

        DummyCheckoutScenario.execute()
        checkoutAction.selectPaymentMethod('Pay by invoice by Sprinque')
        cy.window().should('not.have.property', '_fs_host')
    })
})
